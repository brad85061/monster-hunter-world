VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} Intellisense 
   Caption         =   "Intellisense"
   ClientHeight    =   3705
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   2475
   OleObjectBlob   =   "Intellisense.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "Intellisense"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private listComplete() As String
Private listCompleteUC() As String
Private listFiltered() As String
Private mIndex As Integer
Private mCell As Range

Private Property Let Index(val As Integer)
    mIndex = val
    Select Case val
        Case 0: lbFiltered.List = listComplete
    End Select
End Property

Private Sub HandleItemSelected()
    Dim I As Integer
    'Identify the selected item
    For I = 0 To lbFiltered.ListCount - 1
        If lbFiltered.Selected(I) Then
            If mIndex = 0 Then
                'Write to the cell and add a full stop
                ActiveCell.Value = lbFiltered.List(I)
                'Trigger the next level of dropdown
'                Index = i + 1
'                Me.Hide
                Unload Me
            Else
                'Write to the cell and close the window
                ActiveCell.Value = ActiveCell.Value & lbFiltered.List(I)
'                Me.Hide
                Unload Me
            End If
        End If
    Next

End Sub

Private Sub lbFiltered_KeyPress(ByVal KeyAscii As MSForms.ReturnInteger)
    'Trap the return key event as if 'selected'
    If KeyAscii = vbKeyReturn Then
        HandleItemSelected
    End If
    
    '27 = Escape.
    If (KeyAscii = 27) Then
'        Me.Hide
        Unload Me
    End If
End Sub

Private Sub lbFiltered_MouseUp(ByVal button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)
    'Trap the click event as if 'selected'
    HandleItemSelected
End Sub

Private Sub Search_Change()
    Dim sSearch As String
    Dim sListbox As String
    Dim vValue As Variant
    Dim aSearch() As String
    ReDim aSearch(1 To 1) As String
    Dim I As Integer
    I = 1
    sSearch = UCase(Search.Value)
    For Each vValue In listComplete
        'Current listbox string to check.
        sListbox = UCase(vValue)
        If InStr(sListbox, sSearch) Then
            ReDim Preserve aSearch(1 To UBound(aSearch) + 1) As String
            aSearch(I) = vValue
            Me.lbFiltered.List = aSearch
            I = I + 1
        End If
    Next
    lbFiltered.ListIndex = 0
End Sub

Private Sub UserForm_Activate()
    Application.SendKeys ("{tab}")
    Application.SendKeys ("{tab}")
    
    lbFiltered.ListIndex = 0
    
    Call modPositionIntellisense.positionIntellisense
End Sub

Private Sub UserForm_Initialize()
    Dim Rng As Range
    Dim vArray2D As Variant
    Dim vArray1D As Variant
    Dim sMyArray() As String
    Dim I As Integer
    
    Set Rng = ActiveWorkbook.Names("JewelListSortedAlphaShort").RefersToRange
    vArray2D = Rng.Value
    vArray1D = Application.Transpose(vArray2D)
    
    ReDim sMyArray(1 To UBound(vArray1D)) As String
    ReDim listCompleteUC(1 To UBound(vArray1D)) As String
    For I = 1 To UBound(vArray1D)
      sMyArray(I) = CStr(vArray1D(I))
      listCompleteUC(I) = UCase(vArray1D(I))
    Next
    
    listComplete = sMyArray
    listFiltered = sMyArray

    lbFiltered.List = listFiltered
End Sub

Private Sub Search_KeyPress(ByVal KeyAscii As MSForms.ReturnInteger)
    '27 = Escape.
    If (KeyAscii = 27) Then
'        Me.Hide
        Unload Me
    End If
End Sub
